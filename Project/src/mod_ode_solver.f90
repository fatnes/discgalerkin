!>
!!# Module ODE solver
!!
!! Generic structure for explicit ODE integrators
!!
!! This module defines two types: one to represent an ODE and one to
!! represent an ODE integrator.
!!
!! The ODE is completely defined by its righ-hand-side:
!! \[  
!!  y' = rhs(y)
!! \]
!! An ODE integrator has one method to compte ynew given ynow; there
!! are also two methods to initialize and clean-up the time integrator
!! which can be used to allocate the work arrays needed by the method
!! (if any).
!!
module mod_ode_solver

 use mod_kinds, only: wp
 
 use mod_ode_state, only: c_ode_state

 implicit none

 public :: c_ode, c_ode_integrator

 private

!-----------------------------------------------------------------------------

 ! ODE
 type, abstract :: c_ode
 contains
  procedure(i_compute_rhs), deferred, pass(ode) :: compute_rhs
 end type c_ode

!-----------------------------------------------------------------------------

 abstract interface
  subroutine i_compute_rhs(rhs,ode,y)
   import :: c_ode, c_ode_state
   implicit none
   class(c_ode),       intent(in)    :: ode
   class(c_ode_state), intent(in)    :: y
   class(c_ode_state), intent(inout) :: rhs
  end subroutine i_compute_rhs
 end interface

!-----------------------------------------------------------------------------

 ! ODE integrator
 type, abstract :: c_ode_integrator
 contains
  procedure, pass(ode_int) :: setup => default_setup
  procedure, pass(ode_int) :: clean => default_clean
  procedure(i_compute_step), deferred, pass(ode_int) :: compute_step
 end type c_ode_integrator

!-----------------------------------------------------------------------------

 abstract interface
  subroutine i_compute_step(ynew , ode_int,ode,dt,ynow)
   import :: wp, c_ode_state, c_ode, c_ode_integrator
   implicit none
   class(c_ode_integrator), intent(inout) :: ode_int
   class(c_ode),            intent(in)    :: ode
   real(wp),                intent(in)    :: dt
   class(c_ode_state),      intent(in)    :: ynow
   class(c_ode_state),      intent(inout) :: ynew
  end subroutine i_compute_step
 end interface

!-----------------------------------------------------------------------------

contains

 subroutine default_setup(ode_int,yinit)
  class(c_ode_integrator), intent(inout) :: ode_int
  class(c_ode_state),      intent(in)    :: yinit
   ! This default method does not do anything: override it for
   ! types requiring an initialization.
 end subroutine default_setup

!-----------------------------------------------------------------------------

 subroutine default_clean(ode_int)
  class(c_ode_integrator), intent(inout) :: ode_int
   ! This default method does not do anything: override it for
   ! types requiring an clean-up phase..
 end subroutine default_clean

!-----------------------------------------------------------------------------

end module mod_ode_solver

