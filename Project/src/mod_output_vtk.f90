
!> 
!!# Module Output VTK
!!
!! Module for generic data output in vtk xml fromat
!!
module mod_output_vtk
! MODULES
 use mod_kinds, only: wp

implicit none
private

public::WriteDataToVTK

!-----------------------------------------------------------------------------

contains

!> Subroutine to write 2D/3D point data to binary VTK format
!!
subroutine WriteDataToVTK(dim1,vecdim,nVal,NPlot,nElems,VarNames,Coord,Values,FileString)
 !-----------------------------------------------------------------------------
 ! input variables
 integer,intent(in)            :: dim1                    !! dimension of the data (either 2=quads or 3=hexas)
 integer,intent(in)            :: vecdim                  !! dimension of vector data (found by variable names ending with X,Y,(Z))
 integer,intent(in)            :: nVal                    !! Number of nodal output variables
 integer,intent(in)            :: NPlot                   !! Number of output points .EQ. NAnalyze
 integer,intent(in)            :: nElems                  !! Number of output elements
 real(wp),intent(in)           :: Coord(3,1:(Nplot+1)**dim1,nElems)      !! CoordsVector still 2D!! 
 character(len=*),intent(in)   :: VarNames(nVal)          !! Names of all variables that will be written out
 real(wp),intent(in)           :: Values(nVal,1:(Nplot+1)**dim1,nElems)   !! Statevector 
 character(len=*),intent(in)   :: FileString              !! Output file name
 !-----------------------------------------------------------------------------
 ! local variables
 integer            :: i,j,k,iVal,iElem,Offset,nBytes,nVTKElems,nVTKCells,ivtk=44
 integer            :: Vertex(2**dim1,(NPlot+1)**dim1*nElems)  ! ?
 integer            :: NPlot_p1_3,NPlot_p1_2,NPlot_p1,NodeID,NodeIDElem,ElemType  ! ?
 character(len=35)  :: StrOffset,TempStr1,TempStr2  ! ?
 character(len=300) :: Buffer
 character(len=255) :: tmpVarName,tmpVarNameY,tmpVarNameZ
 integer            :: StrLen,iValVec,nValVec,VecOffset(0:nVal)
 logical            :: isVector,maybeVector
 character(len=1)   :: strvecdim
 character(len=1)   :: lf
 integer            :: INT
 real(kind=4)       :: Float
 integer            :: SIZEOF_F_INT,SIZEOF_F_FLOAT
 !-----------------------------------------------------------------------------
  !WRITE(*,'(A)',ADVANCE='NO')"   WRITE DATA TO VTX XML BINARY (VTU) FILE... "//TRIM(FileString)
  NPlot_p1  =(Nplot+1)
  NPlot_p1_2=Nplot_p1*Nplot_p1
  NPlot_p1_3=NPlot_p1_2*Nplot_p1
  
  SIZEOF_F_INT=STORAGE_SIZE(INT)/8
  SIZEOF_F_FLOAT=STORAGE_SIZE(FLOAT)/8
  
  ! Line feed character
  lf = char(10)
  WRITE(strvecdim,'(I1)') vecdim
  
  ! Write file
  OPEN(UNIT=ivtk,FILE=TRIM(FileString),ACCESS='STREAM')
  ! Write header
  Buffer='<?xml version="1.0"?>'//lf;WRITE(ivtk) TRIM(Buffer)
  Buffer='<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">'//lf;WRITE(ivtk) TRIM(Buffer)
  ! Specify file type
  nVTKElems=NPlot_p1**dim1*nElems
  nVTKCells=NPlot**dim1*nElems
  Buffer='  <UnstructuredGrid>'//lf;WRITE(ivtk) TRIM(Buffer)
  WRITE(TempStr1,'(I16)')nVTKElems
  WRITE(TempStr2,'(I16)')nVTKCells
  Buffer='    <Piece NumberOfPoints="'//TRIM(ADJUSTL(TempStr1))//'" &
         &NumberOfCells="'//TRIM(ADJUSTL(TempStr2))//'">'//lf;WRITE(ivtk) TRIM(Buffer)
  ! Specify point data
  Buffer='      <PointData>'//lf;WRITE(ivtk) TRIM(Buffer)
  Offset=0
  WRITE(StrOffset,'(I16)')Offset
  !accout for vectors: 
  ! if Variable Name ends with an X and the following have the same name with Y and Z 
  ! then it forms a vector variable (X is omitted for the name) 
  
  iVal=0 !scalars
  iValVec=0 !scalars & vectors
  VecOffset(0)=0
  DO WHILE(iVal.LT.nVal)
    iVal=iVal+1
    iValVec=iValVec+1
    tmpVarName=TRIM(VarNames(iVal)) 
    StrLen=LEN(TRIM(tmpVarName))
    maybeVector=(iVal+vecdim-1.LE.nVal)
    isVector=.FALSE.
    IF(maybeVector)THEN
      SELECT CASE(vecdim)
      CASE(2)
        tmpVarNameY=TRIM(VarNames(iVal+1))
        isVector=(      (INDEX(tmpVarName( StrLen:StrLen),"X").NE.0) &
                   .AND.(INDEX(tmpVarNameY(:StrLen),TRIM(tmpVarName(:StrLen-1))//"Y").NE.0))
      CASE(3)
        tmpVarNameY=TRIM(VarNames(iVal+1))
        tmpVarNameZ=TRIM(VarNames(iVal+2)) 
        isVector=(     (INDEX(tmpVarName( StrLen:StrLen),"X").NE.0) &
                  .AND.(INDEX(tmpVarNameY(:StrLen),TRIM(tmpVarName(:StrLen-1))//"Y").NE.0) &
                  .AND.(INDEX(tmpVarNameZ(:StrLen),TRIM(tmpVarName(:StrLen-1))//"Z").NE.0))
  
      END SELECT
    END IF !maybevector
  
    IF(isvector)THEN !variable is a vector!
      tmpVarName=tmpVarName(:StrLen-1)
      Buffer='        <DataArray type="Float32" Name="'//TRIM(tmpVarName)//'" NumberOfComponents="'//strvecdim// &
             &'" format="appended" offset="'//TRIM(ADJUSTL(StrOffset))//'"/>'//lf;WRITE(ivtk) TRIM(Buffer)
      Offset=Offset+SIZEOF_F_INT+vecdim*nVTKElems*SIZEOF_F_FLOAT
      WRITE(StrOffset,'(I16)')Offset
      VecOffset(iValVec)=VecOffset(iValVec-1)+vecdim
      iVal=iVal+vecdim-1 !skip the Y (& Z) components
    ELSE
      Buffer='        <DataArray type="Float32" Name="'//TRIM(tmpVarName)// &
             &'" format="appended" offset="'//TRIM(ADJUSTL(StrOffset))//'"/>'//lf;WRITE(ivtk) TRIM(Buffer)
      Offset=Offset+SIZEOF_F_INT+nVTKElems*SIZEOF_F_FLOAT
      WRITE(StrOffset,'(I16)')Offset
      VecOffset(iValVec)=VecOffset(iValVec-1)+1
    END IF !isvector
  END DO !iVal <=nVal
  nValVec=iValVec
  
  Buffer='      </PointData>'//lf;WRITE(ivtk) TRIM(Buffer)
  ! Specify cell data
  Buffer='      <CellData> </CellData>'//lf;WRITE(ivtk) TRIM(Buffer)
  ! Specify coordinate data
  Buffer='      <Points>'//lf;WRITE(ivtk) TRIM(Buffer)
  Buffer='        <DataArray type="Float32" Name="Coordinates" NumberOfComponents="3'// &
  '" format="appended" offset="'//TRIM(ADJUSTL(StrOffset))//'"/>'//lf;WRITE(ivtk) TRIM(Buffer)
  Offset=Offset+SIZEOF_F_INT+3*nVTKElems*SIZEOF_F_FLOAT
  WRITE(StrOffset,'(I16)')Offset
  Buffer='      </Points>'//lf;WRITE(ivtk) TRIM(Buffer)
  ! Specify necessary cell data
  Buffer='      <Cells>'//lf;WRITE(ivtk) TRIM(Buffer)
  ! Connectivity
  Buffer='        <DataArray type="Int32" Name="connectivity" format="appended" &
           &offset="'//TRIM(ADJUSTL(StrOffset))//'"/>'//lf;WRITE(ivtk) TRIM(Buffer)
  Offset=Offset+SIZEOF_F_INT+2**dim1*nVTKElems*SIZEOF_F_INT
  WRITE(StrOffset,'(I16)')Offset
  ! Offsets
  Buffer='        <DataArray type="Int32" Name="offsets" format="appended" &
           &offset="'//TRIM(ADJUSTL(StrOffset))//'"/>'//lf;WRITE(ivtk) TRIM(Buffer)
  Offset=Offset+SIZEOF_F_INT+nVTKElems*SIZEOF_F_INT
  WRITE(StrOffset,'(I16)')Offset
  ! Elem types
  Buffer='        <DataArray type="Int32" Name="types" format="appended" &
           &offset="'//TRIM(ADJUSTL(StrOffset))//'"/>'//lf;WRITE(ivtk) TRIM(Buffer)
  Buffer='      </Cells>'//lf;WRITE(ivtk) TRIM(Buffer)
  Buffer='    </Piece>'//lf;WRITE(ivtk) TRIM(Buffer)
  Buffer='  </UnstructuredGrid>'//lf;WRITE(ivtk) TRIM(Buffer)
  ! Prepare append section
  Buffer='  <AppendedData encoding="raw">'//lf;WRITE(ivtk) TRIM(Buffer)
  ! Write leading data underscore
  Buffer='_';WRITE(ivtk) TRIM(Buffer)
  
  ! Write binary raw data into append section
  ! Point data
  nBytes = nVTKElems*SIZEOF_F_FLOAT
  DO iValVec=1,nValVec
    WRITE(ivtk) (vecOffset(iValVec)-vecOffset(iValVec-1))*nBytes, &
                REAL(Values(VecOffSet(iValVec-1)+1:VecOffset(iValVec),:,:),4)
  END DO !iValVec
  ! Points
  nBytes = nBytes * 3
  WRITE(ivtk) nBytes
  WRITE(ivtk) REAL(Coord(:,:,:),4)
  ! Connectivity
  SELECT CASE(dim1)
  CASE(2)
    NodeID = 0
    NodeIDElem = 0
    DO iElem=1,nElems
      DO j=1,NPlot
        DO i=1,NPlot
          NodeID = NodeID+1
          !visuQuadElem
          Vertex(:,NodeID) = (/                  &
            NodeIDElem+i+   j   *(NPlot+1)-1,    & !P4
            NodeIDElem+i+  (j-1)*(NPlot+1)-1,    & !P1(CGNS=tecplot standard)
            NodeIDElem+i+1+(j-1)*(NPlot+1)-1,    & !P2
            NodeIDElem+i+1+ j   *(NPlot+1)-1    /) !P3
        END DO
      END DO
      NodeIDElem=NodeIDElem+NPlot_p1_2
    END DO
  CASE(3)
    NodeID=0
    NodeIDElem=0
    DO iElem=1,nElems
      DO k=1,NPlot
        DO j=1,NPlot
          DO i=1,NPlot
            NodeID=NodeID+1
            !
            Vertex(:,NodeID)=(/                                       &
              NodeIDElem+i+   j   *(NPlot+1)+(k-1)*NPlot_p1_2-1,      & !P4(CGNS=tecplot standard)
              NodeIDElem+i+  (j-1)*(NPlot+1)+(k-1)*NPlot_p1_2-1,      & !P1
              NodeIDElem+i+1+(j-1)*(NPlot+1)+(k-1)*NPlot_p1_2-1,      & !P2
              NodeIDElem+i+1+ j   *(NPlot+1)+(k-1)*NPlot_p1_2-1,      & !P3
              NodeIDElem+i+   j   *(NPlot+1)+ k   *NPlot_p1_2-1,      & !P8
              NodeIDElem+i+  (j-1)*(NPlot+1)+ k   *NPlot_p1_2-1,      & !P5
              NodeIDElem+i+1+(j-1)*(NPlot+1)+ k   *NPlot_p1_2-1,      & !P6
              NodeIDElem+i+1+ j   *(NPlot+1)+ k   *NPlot_p1_2-1      /) !P7
          END DO
        END DO
      END DO
      !
      NodeIDElem=NodeIDElem+NPlot_p1_3
    END DO
  END SELECT
  nBytes = 2**dim1*nVTKElems*SIZEOF_F_INT
  WRITE(ivtk) nBytes
  WRITE(ivtk) Vertex(:,:)
  ! Offset
  nBytes = nVTKElems*SIZEOF_F_INT
  WRITE(ivtk) nBytes
  WRITE(ivtk) (Offset,Offset=2**dim1,2**dim1*nVTKElems,2**dim1)
  ! Elem type
  ElemType =3+3*dim1 !9 VTK_QUAD 12  VTK_HEXAHEDRON
  WRITE(ivtk) nBytes
  WRITE(ivtk) (ElemType,iElem=1,nVTKElems)
  ! Write footer
  Buffer=lf//'  </AppendedData>'//lf;WRITE(ivtk) TRIM(Buffer)
  Buffer='</VTKFile>'//lf;WRITE(ivtk) TRIM(Buffer)
  CLOSE(ivtk)
  !WRITE(*,'(A)',ADVANCE='YES')"   DONE"
end subroutine WriteDataToVTK

!-----------------------------------------------------------------------------

end module mod_output_vtk
