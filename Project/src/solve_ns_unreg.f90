!>
!!# Main Program Solve Navier-Stokes
!!
!! Driver for setup and time integartion of the 2D Navier-stokes DG solver.
!!
program solve_ns_unreg

 use mod_kinds,       only: wp
 use mod_rk,          only: t_explicit_euler, t_heun_method
 use mod_base,        only: t_base, new_2d_base
 use mod_grid,        only: t_grid
 use mod_2d_grid,     only: new_2d_grid_unreg
 use mod_ns_state,    only: t_ns_state
 use mod_ns_ode,      only: t_ns_ode, ns_ode_setup, ns_ode_clean
 use mod_ns_eqn,      only: init_ns_eqn,initial_condition
 use mod_readintools, only: GETSTR,GETINT,GETREAL,GETINTARRAY,GETREALARRAY


 implicit none

 !---------------------------------------------------------------------------
 ! local variables
 integer, parameter  :: d = 2
 character(len=255)  :: projectname
 ! mesh parameters
 integer             :: n_elem(2) 
 real(wp)            :: v(5,2)
 integer             :: BCtype(4) !(lower,upper,left,right)
 character(:), allocatable :: domainType
 ! Discretization parameters
 integer             :: degree
 integer             :: n_step 
 real(wp)            :: dt 
 integer             :: output_skip 

 ! Discretization
 type(t_base) :: base
 type(t_grid) :: grid

 ! ODE
 integer :: n
 real(wp) :: t
 type(t_ns_state) :: ynow, ynew
 type(t_ns_ode)   :: ode

 ! Time integrator
 type(t_heun_method) :: time_integrator

 ! initialization
 integer :: ie
 real(wp), allocatable :: xdata(:,:,:)
 real(wp) :: time_start,time_end
 integer :: iout
 !---------------------------------------------------------------------------

  ! Set-up the problem

  write(*,'(100("="))')
  projectname  = GETSTR('projectname','DG_solution')
  !discretization
  degree  = GETINT('degree','0')
  n_step  = GETINT('n_step','500')
  dt      = GETREAL('dt','0.0015d0')
  output_skip  = GETINT('output_skip','50')

  !mesh
  write(*,'(100("-"))') 
  n_elem  = GETINTARRAY('n_elem',2,'40,40')
 ! x_left  = GETREALARRAY('x_lower_left',2,'0.0d0,0.0d0')
 ! x_right = GETREALARRAY('x_upper_right',2,'2.0d0,2.0d0')
  v(1,:) = GETREALARRAY('v1',2, '0.0d0,0.0d0') 
  v(2,:) = GETREALARRAY('v2',2, '2.0d0,0.0d0') 
  v(3,:) = GETREALARRAY('v3',2, '2.0d0,2.0d0') 
  v(4,:) = GETREALARRAY('v4',2, '0.0d0,2.0d0') 
  v(5,:) = GETREALARRAY('v5',2, '0.0d0,0.0d0') 
 
  allocate(domainType, source =GETSTR('domainType', 'rectangular')) 
 ! domainType = 'parabolic'
 
  BCtype(1)=GETINT('BCtype_lower','10')
  BCtype(2)=GETINT('BCtype_upper','10')
  BCtype(3)=GETINT('BCtype_left','10')
  BCtype(4)=GETINT('BCtype_right','10')

  !check periodicity
  if((BCtype(2).ne.BCtype(1)).and.(any(BCtype(1:2).eq.10))) then
    stop 'for periodic, both BCtype_lower & BCtype_upper =10!'
  end if
  if((BCtype(4).ne.BCtype(3)).and.(any(BCtype(3:4).eq.10))) then
    stop 'for periodic, both BCtype_left & BCtype_right =10!'
  end if


  !initialize equation variables
  call init_ns_eqn(BCType)

  ! grid
  select case(d)
   case(2) ! two-dimensional
    call new_2d_base(base,degree)
    call new_2d_grid_unreg(grid,n_elem,v, domainType, BCtype)
   case default
    error stop 'In "solve_ns.f90": only d=2 is implemented!'
  end select

  ! define the ODE
  call ns_ode_setup( ode, d, base, grid)
  ! the grid and the base have been copied into ode: we can free them
  call base%clean()
  call grid%clean()

  ! prepare the state vectors
  call ode%new_ns_state( ynow )
  call ode%new_ns_state( ynew )

  ! initial condition and visualization
  associate(  n_coords => size(ode%base%x_dofs,1) , &
             n_out_pts => size(ode%base%x_dofs,2) )
  allocate( xdata(n_coords,n_out_pts,ode%grid%ne) )
  do ie=1,ode%grid%ne
    call ode%grid%e(ie)%map( xdata(:,:,ie) , ode%base%x_dofs )
    ynow%uu(:,:,ie) = initial_condition( xdata(:,:,ie) )
  end do
  end associate

  !visualize initialization
  iout=0
  call visualize_to_vtk( projectname, iout , ynow%uu(:,:,:) )

  ! Integrate the system
  call time_integrator%setup(ynow)

  call cpu_time(time_start)
  write(*,'(100("="))') 
  write(*,*) "  Start simulation ..."

  do n=1,n_step
    t = real(n,wp)*dt

    ! Compute the time step
    call time_integrator%compute_step(ynew , ode,dt,ynow)
    call ynow%copy( ynew )

    ! Write the solution
    if(mod(n,output_skip).eq.0) then
      iout=iout+1
      write(*,'(I4,A,E12.6,A,I6,A,I6)' )iout,"   t_sim: ",t,' , timesteps: ',n," / ",n_step
      if(any(ISNAN(ynow%uu))) stop "ABORT: nan encountered!!!"
      call visualize_to_vtk( projectname, iout , ynow%uu(:,:,:) )
    end if

  end do

  call cpu_time(time_end)
  write(*,'(100("="))') 
  write(*,'(A,F8.1,A)') ' ...done after [',time_end-time_start, &
             ' s ] ,results written to '//TRIM(projectname)//'*.vtu files.'
  write(*,'(100("="))') 

  ! Clean-up
  deallocate( ynow%uu )
  deallocate( ynew%uu )
  deallocate( xdata )
  call ns_ode_clean(ode)
  call time_integrator%clean()

contains

!-----------------------------------------------------------------------------

 !> prepare writing of  solution for visualization to vtk file
 !! visualization poitns are equidistant in reference space and
 !! elements are sampled with (visudeg+1)^2 visualization points
 !!
 subroutine visualize_to_vtk(filename,iter,uu)
  !---------------------------------------------------------------------------
  use mod_base, only: eval_2d_phi
  use mod_ns_eqn, only: cons_to_prim
  use mod_output_vtk, only: writeDataToVTK
  !---------------------------------------------------------------------------
  !input variables
  character(len=*)    :: Filename
  integer, intent(in) :: iter
  real(wp), intent(in) :: uu(2+d,(degree+1)**d,ode%grid%ne)
  !---------------------------------------------------------------------------
  !local variables
  integer              :: visuDeg,np_visu
  integer              :: i,j,ie
  real(wp),allocatable :: xdata3D(:,:,:)
  real(wp),allocatable :: uu_out(:,:,:)
  real(wp),allocatable :: xiVisu(:,:)
  real(wp),allocatable :: phiVisu(:,:)
  character(len=200)   :: FileString
  character(len=50)    :: VarNames(4)
  !---------------------------------------------------------------------------
   if(d.ne.2) return !only written to 2-dimensional data
   visuDeg=1
   np_visu=(visudeg+1)**2
   allocate(xiVisu(2,np_visu))
   allocate(phiVisu(ode%base%n,np_visu))
   allocate(xdata3D(3,np_visu,ode%grid%ne))
   allocate(uu_out(4,np_visu,ode%grid%ne))
   do j=0,visudeg
     do i=0,visudeg
       xiVisu(1:2,1+i+(visudeg+1)*j)=2.0_wp*[REAL(i,wp),REAL(j,wp)]/REAL(visuDeg,wp)-1.0_wp
     end do
   end do
   !evaluate basis at visualization points
   do i=1,np_visu
     phiVisu(:,i)=eval_2d_phi(ode%base%n,xiVisu(:,i))
   end do
   !evaluate mesh points on each element
   do ie=1,ode%grid%ne
     call ode%grid%e(ie)%map( xdata3D(1:2,:,ie) , xiVisu(:,:) )
   end do !ie
   xdata3D(3,:,:)=0.0_wp
   
   write(FileString,'(A,A1,I0.6,A)')TRIM(filename),'_',iter,'.vtu'
   VarNames(1)='Density'
   VarNames(2)='VelocityX'
   VarNames(3)='VelocityY'
   VarNames(4)='Pressure'
   
   !evaluate solution at visualization points and convert to primitive variables
   do ie=1,ode%grid%ne
     do i=1,np_visu
       uu_out(:,i,ie) =cons_to_prim(matmul(uu(:,:,ie),phiVisu(:,i)))
     end do
   end do
   call WriteDataToVTK(2,2,4,visuDeg,ode%grid%ne,VarNames(1:4),xdata3D,uu_out(1:4,:,:),FileString)

 end subroutine visualize_to_vtk

!-----------------------------------------------------------------------------

end program solve_ns_unreg

