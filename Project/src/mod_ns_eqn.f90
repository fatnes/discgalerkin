!>
!!# Module Navier-Stokes Equation
!!
!! provides the routines for the Navier-Stokes ODE module and the gradient evaluation ( marked by `g_`):  
!!
!!- initialzation of variables and boundary conditions
!!- euler and viscous flux
!!- numerical flux
!!- inner and boundary fluxes  
!!- initial condition (using exact function)
!!- conversion between conservative and primitive variables
!!
module mod_ns_eqn

 use mod_kinds, only: wp

 implicit none

 public 

!-----------------------------------------------------------------------------
!module variables
 integer , parameter  :: d=2 

 real(wp)      :: cp                !! heat capacity, default =1004.67
 real(wp)      :: gamma             !! =cp/cv, default=1.4
 real(wp)      :: cv                !! =cp/gamma
 real(wp)      :: rgas              !! =cp-cv
 real(wp)      :: mu                !! viscosity
 real(wp)      :: alpha             !! =mu*cp/Prandtl
 integer       :: inifunc           !! function for initialization
 real(wp)      :: iniStatePrim(2+d) !! constant state for initialization
 integer       :: BCtype(4)         !! index of boundary conditions (lower, upper,left,right) 
 real(wp)      :: BCstate(2+d,4)    !! constant state for boundaries

 !> Parameter \(\eta\) for the stabilization of the diffusive flux
 real(wp), parameter  :: eta = 0.5_wp


!-----------------------------------------------------------------------------

contains

!-----------------------------------------------------------------------------
 !> Initialize module variables with data from parameterfile
 !!
 subroutine init_ns_eqn(BCtype_in) 
  use mod_readintools, only: GETINT,GETREAL,GETREALARRAY 
  !---------------------------------------------------------------------------
  integer,intent(in) :: BCtype_in(4)
  !local variables
  real(wp)           :: Pr !Prandtl number 
  !---------------------------------------------------------------------------
  write(*,'(100("-"))') 
  write(*,'(A)') 'initialize equation variables...'
  cp      = GETREAL('cp','1004.670d0')
  gamma   = GETREAL('gamma','1.40d0')
  cv      = cp/gamma
  rgas    = cp-cv
  mu      = GETREAL('mu','0.0d0')
  Pr      = GETREAL('Pr','0.720d0')
  alpha   = mu*cp/Pr
  inifunc = GETINT('inifunc','1')

  inistatePrim=-1.0_wp 
  !inistate mandatory only if inifunc=0
  if ( inifunc.eq.0 ) iniStatePrim = GETREALARRAY('iniState',2+d)

  BCtype=BCtype_in
  BCstate(:,:)=-1.0_wp
  if(BCtype(1).eq.20) BCstate(:,1)=prim_to_cons(GETREALARRAY('BCstate_lower',2+d))
  if(BCtype(2).eq.20) BCstate(:,2)=prim_to_cons(GETREALARRAY('BCstate_upper',2+d))
  if(BCtype(3).eq.20) BCstate(:,3)=prim_to_cons(GETREALARRAY('BCstate_left' ,2+d))
  if(BCtype(4).eq.20) BCstate(:,4)=prim_to_cons(GETREALARRAY('BCstate_right',2+d))

 end subroutine init_ns_eqn

!-----------------------------------------------------------------------------
 
 !> Compute primitive state from conservative state
 !!
 pure function cons_to_prim(cons) RESULT(prim)
  !---------------------------------------------------------------------------
  real(wp),intent(in) :: cons(2+d)
  real(wp)            :: prim(2+d)
  !---------------------------------------------------------------------------
   prim(1)=cons(1)
   prim(2:1+d)=cons(2:1+d)/cons(1)
   prim(2+d) = (gamma-1.0_wp)*(cons(2+d)-0.5_wp*sum(prim(2:1+d)*cons(2:1+d)))
 end function cons_to_prim

!-----------------------------------------------------------------------------
 
 !> Compute conservative state from primitive state
 !!
 pure function prim_to_cons(prim) RESULT(cons)
  !---------------------------------------------------------------------------
  real(wp),intent(in) :: prim(2+d)
  real(wp)            :: cons(2+d)
  !---------------------------------------------------------------------------
   cons(1)=prim(1)
   cons(2:1+d)=prim(2:1+d)*prim(1)
   cons(2+d) = prim(2+d)/(gamma-1.0_wp) + 0.5_wp*sum(prim(2:1+d)*cons(2:1+d))
 end function prim_to_cons

!-----------------------------------------------------------------------------
 
 !> Compute psi=(v,T) from conservative state
 !!
 pure function cons_to_psi(cons) RESULT(psi)
  !---------------------------------------------------------------------------
  real(wp),intent(in) :: cons(2+d)
  real(wp)            :: psi(1+d)
  !---------------------------------------------------------------------------
   psi(1:d)=cons(2:1+d)/cons(1)
   psi(1+d) = 1.0_wp/cv*( cons(2+d)/cons(1) - 0.5_wp*dot_product(psi(1:d),psi(1:d)) )
 end function cons_to_psi

!-----------------------------------------------------------------------------

 !> initialize solution at x(1:2,n) point positions
 !!
 pure function initial_condition(x) result(uu0)
  !---------------------------------------------------------------------------
  !input variable
  real(wp), intent(in) :: x(:,:) !! x,y positions
  !output variable
  real(wp) :: uu0(2+d,size(x,2)) !! \(\rho,\rho v_1,\rho v_2,E\) at x,y positions
  !---------------------------------------------------------------------------
  !local variables 
  integer :: i
  !---------------------------------------------------------------------------

   do i=1,size(x,2)
     uu0(:,i)= prim_to_cons( exact_function(inifunc,x(:,i),0.0_wp) )
   end do

 end function initial_condition

!-----------------------------------------------------------------------------

 !> evaluate analytic function of x(1:2) and give primitive state back
 !!
 pure function exact_function(exfunc,x,t) result(prim)
  !---------------------------------------------------------------------------
  !input variables
  integer,  intent(in) :: exfunc
  real(wp), intent(in) :: x(2), t
  !output variable
  real(wp) :: prim(4)
  !---------------------------------------------------------------------------
  !local variables
  real(wp), parameter :: pi = 3.14159265358979323846264338327950288_wp
  real(wp) :: du,dT
  !---------------------------------------------------------------------------
   associate(rho=>prim(1), &
             vel=>prim(2:3), &
             pres=>prim(4)) 
   select case(exfunc)
   case(0) !inistate
     prim(:)=iniStatePrim(:) !primitive variables

   case(1) !sinusodial density, only transported, v=(0.5,0.5), c=1
     rho = 1.1_wp*(1.0_wp+ 0.1_wp*sin( 2.0_wp*pi * SUM(x(:)-vel(:)*t)) )
     vel  =[ 0.5_wp, 0.5_wp]
     pres = 1.1_wp/1.4_wp

   case(2) ! pressure pulse at 0.7,0.6
     associate( dx => x(1:2)-[0.7_wp,0.6_wp] )
     rho=1.0_wp
     vel=0.0_wp
     pres  = 1.0_wp+ 0.2_wp*exp(-SUM(dx(:)**2)/0.025_wp)
     end associate !dx

   case(3) !shear flow
     associate( delta => 0.25_wp*sin(0.5_wp*pi*x(2))**8 &
                        *exp(-(x(2)-1.0_wp)**2/0.1_wp) )
     rho        = 1.0_wp + delta
     vel(1) = 4.0_wp*delta
     vel(2) = 0.1_wp*sin(pi*x(1))
     pres   = (gamma-1.0_wp)
     end associate !delta
   case(4) !isentropic vortex
     associate ( rho_0 => 1.0_wp, u_0=>0.5_wp, v_0=>0.5_wp, p_0=>1.0_wp, &
                 A=>0.1_wp, H=>0.2_wp,xc=>[1.0_wp,1.0_wp] )
     associate (xi => (x(:)-xc(:))/H)

     du     = A*exp(0.5_wp*(1.0_wp-dot_product(xi,xi)))
     dT     = (gamma-1.0_wp)*rho_0/(2.0_wp*gamma*p_0)*du*du
     rho    = (1.0_wp-dT)**(1.0_wp/(gamma-1.0_wp))
     vel(1) = u_0 - du *xi(2)
     vel(2) = v_0 + du *xi(1)
     pres   = p_0*rho**(gamma)
     rho    = rho_0*rho

     end associate
     end associate
    
   case default
     rho=1.0_wp
     vel=0.0_wp
     pres=1.0_wp
   end select !case(exfunc)
   end associate !rho,vel,pres
 end function exact_function

!-----------------------------------------------------------------------------

 !> Evaluate the boundary flux depending on the type of boundary
 !!
 function bc_flux(d,ms,BC_ID,n,uu,gg) 
  !---------------------------------------------------------------------------
  !input variables
  integer, intent(in) :: d            !! dimension
  integer, intent(in) :: ms           !! number of points
  integer, intent(in) :: BC_ID        !! index of boundary condition
  real(wp),intent(in) :: n(d,ms)      !! outward pointing normal vector
  real(wp),intent(in) :: uu(2+d,ms)   !! inner boundary state (not rotated!)
  real(wp),intent(in) :: gg(d,d+1,ms) !! inner boundary gradient of (v,T) (not rotated!)
  !output variable
  real(wp)            :: bc_flux(2+d,ms) !! normal boundary flux
  !---------------------------------------------------------------------------
  !local variables
  integer     :: i,j,q
  real(wp)    :: v(d), p, div_v, tau(d,d)
  !---------------------------------------------------------------------------
 
  select case(BCtype(BC_ID))
  case(20) !Dirichlet of constant BCstate, weakly imposed (characteristics) via riemann solver
           ! all gradients from inside
    do q=1,ms
      bc_flux(:,q) = euler_num_flux(n(:,q),uu(:,q),BCstate(:,BC_ID))        &
                           +  matmul(n(:,q), viscous_flux(BCstate(:,BC_ID),gg(:,:,q)))
    end do !q=1,ms

  case(31) !slip wall v*n=0 (symmetry BC)
    do q=1,ms
      associate( rho => uu(1,q),       rhov => uu(1+1:1+d,q), &
                   e => uu(d+2,q)     )
      v = rhov/rho
      p = (gamma-1.0_wp)*(e-0.5_wp*dot_product(rhov,v))

      !symmetry: no viscous contribution (normal derivatives =zero):

      bc_flux(  1  ,q) = 0.0_wp     !mass
      bc_flux(2:d+1,q) = p*n(:,q)   !momentum
      bc_flux( d+2 ,q) = 0.0_wp     !energy
      end associate !rho,rhov,e
    end do !q=1,ms

  case(32) !no-slip wall v=0, adiabatic (T=T_inner)
    do q=1,ms
      associate( rho => uu(1,q),       rhov => uu(1+1:1+d,q), &
                   e => uu(d+2,q) ,    grad_v => gg(:,1:d,q)   )
      v = rhov/rho
      p = (gamma-1.0_wp)*(e-0.5_wp*dot_product(rhov,v))

      div_v = 0.0_wp
      do j=1,d
        div_v = div_v + grad_v(j,j)
      enddo
      do j=1,d
        do i=1,d
          tau(i,j) = -mu*( grad_v(i,j)+grad_v(j,i) )
        end do
        ! diagonal contributions
        tau(j,j) = tau(j,j) + 2.0_wp/3.0_wp*mu*div_v
      end do

      bc_flux(  1  ,q) = 0.0_wp                              !mass
      bc_flux(2:d+1,q) = p*n(:,q) + matmul( tau , n(:,q) ) !momentum
      bc_flux( d+2 ,q) = 0.0_wp                              !energy
      end associate !rho,rhov,e,grad_v
    end do !q=1,ms
  case default
    write(*,*)'BC not implemented in bc_flux, BCtype=',BCtype(BC_ID)
    stop
  end select !BCtype


 end function bc_flux 

!-----------------------------------------------------------------------------

 !> Compute the boundary flux for the gradient equation,
 !! depending on the boundar type.
 !!
 function g_bc_flux(d,ms,BC_ID,n,uu) 
  !---------------------------------------------------------------------------
  !input variables
  integer, intent(in) :: d                 !! dimension
  integer, intent(in) :: ms                !! number of points
  integer, intent(in) :: BC_ID        !! index of boundary condition
  real(wp),intent(in) :: n(d,ms)           !! outward pointing normal vector
  real(wp),intent(in) :: uu(2+d,ms)        !! inner boundary state (not rotated!)
  !output variable
  real(wp)            :: g_bc_flux(d+1,ms) !! normal boundary flux for gradient
  !---------------------------------------------------------------------------
  !local variables
  integer             :: q
  real(wp)            :: v(d),T
  !---------------------------------------------------------------------------

  select case(BCtype(BC_ID))
  case(20) !Dirichlet with constant BCstate
    associate(   rho => BCstate(1    ,BC_ID) , &
               rho_v => BCstate(2:1+d,BC_ID) , &
                   e => BCstate(d+2  ,BC_ID)   )
    v = rho_v/rho
    T = 1.0_wp/cv*( e/rho - 0.5_wp*dot_product(v,v) ) 
    end associate
    do q=1,ms
      g_bc_flux(1:d,q)=v
      g_bc_flux(d+1,q)=T
    end do !q=1,ms

  case(31) !slip wall (symmetry BC), v*n=0
    do q=1,ms
      associate(   rho => uu(1,q) ,  &
                 rho_v => uu(2:1+d,q) , &
                     e => uu(d+2,q) )
      v = rho_v/rho
      T = 1.0_wp/cv*( e/rho - 0.5_wp*dot_product(v,v) ) 
      end associate
      g_bc_flux(1:d,q)=v-n(:,q)*dot_product(v,n(:,q)) !v*n=0
      g_bc_flux(d+1,q)=T
    end do !q=1,ms

  case(32) !no-slip adiabatic wall, vel=0, temperature from inside
    do q=1,ms
      associate(   rho => uu(1,q) ,  &
                 rho_v => uu(2:1+d,q) , &
                     e => uu(d+2,q) )
      v = rho_v/rho
      T = 1.0_wp/cv*( e/rho - 0.5_wp*dot_product(v,v) ) 
      end associate
      g_bc_flux(1:d,q)=0.0_wp
      g_bc_flux(d+1,q)=T
    end do !q=1,ms
   case default
    write(*,*)'BC not implemented in g_bc_flux,BCtype=',BCtype(BC_ID)
    stop
  end select !BCtype
 end function g_bc_flux

!-----------------------------------------------------------------------------

 !>  evaluate the euler flux
 !! \[
 !!    \V{F}(U) =\Mat{\rho \V{v} \\ \rho \V{v}\V{v}^T + I p\\ (E+p)\V{v} \\} 
 !! \]
 !! where \(U=(\rho,\rho \V{v}, E) \)
 !!
 pure function euler_flux(uu) result(flux)
  !---------------------------------------------------------------------------
  !input variables
  real(wp), intent(in) :: uu(:)
  !output variable
  real(wp)             :: flux( size(uu)-2 , size(uu) )
  !---------------------------------------------------------------------------
  !local variables
  integer  :: d, i, j
  real(wp) :: v(size(uu)-2), p ! velocity, pressure
  !---------------------------------------------------------------------------

   d = size(uu)-2 ! space dimension

   associate( rho  => uu(1),       & 
              rhov => uu(1+1:1+d), &
              e    => uu(d+2)      )

   v = rhov/rho
   p = (gamma-1.0_wp)*(e-0.5_wp*dot_product(rhov,v))

   flux(:,1)   = rhov
   do j=1,d
     do i=1,d
       flux(i,1+j) = rhov(i)*v(j)
     enddo
     flux(j,1+j) = flux(j,1+j) + p ! diagonal contributions
   enddo
   flux(:,d+2) = v*(e+p) 

   end associate
  
 end function euler_flux

!-----------------------------------------------------------------------------

 !>  evaluate the viscous flux of the navier-stokes equations
 !! \[
 !!    \V{F}(U) =\Mat{\V{0} \\ \tau \\ q+ \tau\cdot\V{v} \\} 
 !! \] 
 !!
 pure function viscous_flux(uu,gg) result(flux)
  !---------------------------------------------------------------------------
  !input variables
  real(wp), intent(in) :: uu(:)   !! \(U\)
  real(wp), intent(in) :: gg(:,:) !! \(\T{G}\)
  !output variable
  real(wp)             :: flux( size(uu)-2 , size(uu) )
  !---------------------------------------------------------------------------
  !local variables
  integer  :: d, i, j
  real(wp) :: v(size(uu)-2), div_v
  real(wp) :: tau(size(uu)-2,size(uu)-2), q(size(uu)-2)
  !---------------------------------------------------------------------------

   d = size(uu)-2 ! space dimension
   associate( grad_v=>gg(:,1:d) , grad_t=>gg(:,d+1) )

   ! compute v
   v = uu(2:1+d)/uu(1)

   ! compute div(v)
   div_v = 0.0_wp
   do j=1,d
     div_v = div_v + grad_v(j,j)
   enddo

   ! compute tau
   do j=1,d
     do i=1,d
       tau(i,j) = -mu*( grad_v(i,j)+grad_v(j,i) )
     enddo
     ! diagonal contributions
     tau(j,j) = tau(j,j) + 2.0_wp/3.0_wp*mu*div_v
   enddo

   ! compute q
   q = -alpha*grad_t

   ! compute the fluxes
   flux(:, 1   ) = 0.0_wp
   flux(:,2:1+d) = tau
   flux(:, 2+d ) = q + matmul( tau , v )

   end associate

 end function viscous_flux

!-----------------------------------------------------------------------------

 !> evaluate the numerical flux for the inviscid euler part
 !! \[
 !!  \widehat{F}_n=F_\text{RP}(U_L,U_R,\V{n}) 
 !! \]
 !! The simplest numerical flux, the Lax-Friedrichs flux, writes as
 !! \[
 !!  \widehat{F}_n=\frac{1}{2}\left(F_{euler}(U_L)+F_{euler}(U_R)\right) 
 !!                - \frac{\lambda_\text{max}}{2}\left (U_R-U_L \right)
 !! \]
 !! where \(\lambda_\text{max}\) is the absolute maximum wavespeed, computed as
 !! \[
 !! \lambda_\text{max}=\max(|\V{v}\cdot\V{n}_L|+ c_L \, , \, |\V{v}\cdot\V{n}_R|+c_R) 
 !! \]
 !! with the sound speed \( c= \sqrt{\gamma \frac{p}{\rho}} \).
 !!
 pure function euler_num_flux(n,uu_l,uu_r) result(f_hat_n)
  !---------------------------------------------------------------------------
  !input variables
  real(wp), intent(in) :: n(:)
  real(wp), intent(in) :: uu_l(:)
  real(wp), intent(in) :: uu_r(:)
  !output variable
  real(wp)             :: f_hat_n(size(uu_l))
  !---------------------------------------------------------------------------
  !local variables
  integer  :: d
  real(wp) :: v_l(size(n)), p_l, c_l ! vel. pressure, sound speed
  real(wp) :: v_r(size(n)), p_r, c_r
  real(wp) :: v_l_n, v_r_n           ! normal velocities
  real(wp) :: lam_max                ! maximum wavespeed
  real(wp) :: ef_l(size(n),size(uu_l)) ! Euer flux
  real(wp) :: ef_r(size(n),size(uu_l))
  !---------------------------------------------------------------------------

  d = size(n) ! space dimension

  associate( rho_l => uu_l(1),        rho_r => uu_r(1),       & 
            rhov_l => uu_l(1+1:1+d), rhov_r => uu_r(1+1:1+d), &
               e_l => uu_l(d+2),        e_r => uu_r(d+2)      )

  v_l = rhov_l/rho_l
  v_r = rhov_r/rho_r
  p_l = (gamma-1.0_wp)*(e_l-0.5_wp*dot_product(rhov_l,v_l))
  p_r = (gamma-1.0_wp)*(e_r-0.5_wp*dot_product(rhov_r,v_r))
  c_l = sqrt(gamma*p_l/rho_l)
  c_r = sqrt(gamma*p_r/rho_r)

  v_l_n = dot_product( v_l , n )
  v_r_n = dot_product( v_r , n )
  lam_max = max( abs(v_l_n)+c_l , abs(v_r_n)+c_r )

  end associate

  ef_l = euler_flux(uu_l)
  ef_r = euler_flux(uu_r)

  f_hat_n = 0.5_wp*matmul(n,ef_l+ef_r) - lam_max*(uu_r-uu_l)
 
 end function euler_num_flux

!-----------------------------------------------------------------------------

 !> Evaluate the numerical flux for the viscous part of the navier-stokes 
 !! equations, which is simply the mean value of left and right viscous flux
 !! and a stabilization term depending on the jump of the solution
 !! 
 pure function viscous_num_flux(n, uu_l,uu_r, gg_l,gg_r)  result(f_hat_n)
  !---------------------------------------------------------------------------
  !input variables
  real(wp), intent(in) :: n(:)
  real(wp), intent(in) :: uu_l(:)
  real(wp), intent(in) :: uu_r(:)
  real(wp), intent(in) :: gg_l(:,:)
  real(wp), intent(in) :: gg_r(:,:)
  !output variable
  real(wp)             :: f_hat_n(size(uu_l))
  !---------------------------------------------------------------------------
  !local variables
  integer  :: d
  real(wp) :: flux_l( size(uu_l)-2 , size(uu_l) )
  real(wp) :: flux_r( size(uu_l)-2 , size(uu_l) )
  !---------------------------------------------------------------------------

   d = size(uu_l)-2 ! spatial dimension

   flux_l = viscous_flux(uu_l,gg_l)
   flux_r = viscous_flux(uu_r,gg_r)

   f_hat_n = 0.5_wp*matmul(n , flux_l+flux_r)
   f_hat_n(2:d+1) = f_hat_n(2:d+1) +    mu*eta*(uu_l(2:d+1)-uu_r(2:d+1))
   f_hat_n( d+2 ) = f_hat_n( d+2 ) + alpha*eta*(uu_l( d+2 )-uu_r( d+2 ))

 end function viscous_num_flux

!-----------------------------------------------------------------------------

end module mod_ns_eqn

