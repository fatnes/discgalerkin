!> 
!!# Module Navier-Stokes state
!!
!! Define state vector for the Navier-Stokes equation as an array with 3 dimensions 
!! (variables,degrees of freedom in the element, elements)
!!
module mod_ns_state

 use mod_kinds, only: wp

 use mod_ode_state, only: c_ode_state

 implicit none

 public :: t_ns_state

 private

!-----------------------------------------------------------------------------

 !> State vector for the Navier-Stokes equation
 type, extends(c_ode_state) :: t_ns_state
  !> \(\code{uu}(:,i,ie) = \left[
  !!  \begin{array}{c}
  !!   \rho_i^{K_{ie}} \\
  !!   \V{v}_i^{K_{ie}} \\
  !!   E_i^{K_{ie}}
  !!  \end{array}
  !! \right]\)
  real(wp), allocatable :: uu(:,:,:)
 contains
  procedure, pass(x) :: increment       => increment
  procedure, pass(x) :: scalar_multiply => scalar_multiply
  procedure, pass(y) :: copy            => copy
  procedure, pass(x) :: clone           => clone
 end type t_ns_state

!-----------------------------------------------------------------------------

contains

 subroutine increment(x,y)
  !---------------------------------------------------------------------------
  class(c_ode_state), intent(in)    :: y
  class(t_ns_state),  intent(inout) :: x
  !---------------------------------------------------------------------------

   select type(y); type is(t_ns_state)
   x%uu = x%uu + y%uu
   end select
 end subroutine increment

!-----------------------------------------------------------------------------

 subroutine scalar_multiply(x,r)
  !---------------------------------------------------------------------------
  real(wp),          intent(in)    :: r
  class(t_ns_state), intent(inout) :: x
  !---------------------------------------------------------------------------

   x%uu = r*x%uu
 end subroutine scalar_multiply

!-----------------------------------------------------------------------------

 subroutine copy(y,x)
  !---------------------------------------------------------------------------
  class(c_ode_state), intent(in)    :: x
  class(t_ns_state),  intent(inout) :: y
  !---------------------------------------------------------------------------

   select type(x); type is(t_ns_state)
   y%uu = x%uu
   end select
 end subroutine copy

!-----------------------------------------------------------------------------

 subroutine clone(y,x)
  !---------------------------------------------------------------------------
  class(t_ns_state), intent(in)  :: x
  class(c_ode_state), allocatable, intent(out) :: y
  !---------------------------------------------------------------------------

   allocate( y , source=x )
 end subroutine clone

!-----------------------------------------------------------------------------

end module mod_ns_state

