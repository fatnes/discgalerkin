!>
!!# Module 2D Grid
!!
!! Build the 2D grid data structure and geometry
!!
module mod_2d_grid

 use mod_kinds, only: wp

 use mod_grid, only: t_grid, c_s, c_e
 
 implicit none

 public ::  new_2d_grid_unreg
 ! The following should be used only for testing
 public :: t_s2d, t_e2d_unreg, map_2d_unreg, &
           get_j_it_2d_unreg, get_detj_2d_unreg

 private

!-----------------------------------------------------------------------------

!module variables
 !> Two-dimensional side
 !!
 type, extends(c_s) :: t_s2d
  real(wp) :: n(2)   !! unit normal \(\V{n}_e\)
  real(wp) :: length !! side length \(|e|\)
 contains
  procedure, pass(s) :: get_n     => get_n_2d
  procedure, pass(s) :: get_sdetj => get_sdetj_2d
 end type t_s2d


 !> Quadrilateral element 
 !! Defined by its four vertices, contained in  \(\V{v}\) 
 !! Each vertex \(i\) is defined by it's coordinates \(v(i,:) = (v_{i,1}, v_{i,2})\) 
 type, extends(c_e) :: t_e2d_unreg
  real(wp) :: v(4,2) ! v(i,j) is the coordinate of vertex i in dimension j 
 contains 
  procedure, pass(e) :: get_j_it => get_j_it_2d_unreg
  procedure, pass(e) :: get_detj => get_detj_2d_unreg
  procedure, pass(e) :: map      => map_2d_unreg
 end type t_e2d_unreg

 !> \(\Delta \tilde{x}_1 = \Delta \tilde{x}_2\) for the reference
 !! element
 real(wp), parameter :: delta_x_tilde = 2.0_wp

 !> Length of the reference side \(\tilde{\tilde{e}}=[-1,1]\)
 real(wp), parameter :: length_e_tilde = 2.0_wp


!-----------------------------------------------------------------------------

contains

!---------------- BUILD GRID ----------------------------

  !> Builds the grid datastructure for a 2D domain. 
  !! It generates elements on a reference domain \([0,1]^2\), and maps these 
  !! to a domain specified by the domain type.
 subroutine new_2d_grid_unreg(grid, ne, domainParameters, domainType, BCtype)
  !---------------------------------------------------------------------------
  !input variables
  integer,           intent(in) :: ne(2)                   !! number of elements in x,y on referenxe domain
  real(wp),          intent(in) :: domainParameters(:,:)   !! Parameters that define the domain
  character(len=*),  intent(in) :: domainType              !! Type of domain, e.g. rectangle or cylinder
  integer,           intent(in) :: BCtype(4)               !! lower,upper,left,right BC
  !output variable
  type(t_grid), intent(out) :: grid
  !---------------------------------------------------------------------------
  !local variables
  integer :: i,is, ie1, ie2, ie, iloc
  !---------------------------------------------------------------------------
   grid%ne = ne(1)*ne(2)

   grid%ns = 2*ne(1)*ne(2) ! periodic box
   if(BCtype(1).ne.10)then !lower/upper not periodic
     grid%ns=grid%ns+ne(1)
   end if
   if(BCtype(3).ne.10)then !left/right not periodic
     grid%ns=grid%ns+ne(2)
   end if

   ! allocate and define the side array
   allocate( t_s2d::grid%s( grid%ns ) )
   select type( sides => grid%s ); type is(t_s2d)


   is=0 !side counter
  ! 1) sides with normal parallel to the x1 axis
   do ie2=1,ne(2)
     !left BC
     ie1=1
     ie = ie1 + (ie2-1)*ne(1)
     if(BCtype(3).eq.10)then !left BC is periodic
       is=is+1
       sides(is)%e_l = ie2*ne(1)             ! left neighbor (periodic element)
       sides(is)%e_r = ie                    ! current element
       sides(is)%iloc_l = 2
       sides(is)%iloc_r = 4
       sides(is)%orient_l = +1
       sides(is)%orient_r = -1
     else
       is=is+1
       sides(is)%e_l = ie                    ! current element
       sides(is)%e_r = -3                    ! NEGATIVE: points to BCtype(3)
       sides(is)%iloc_l = 4
       sides(is)%iloc_r = -9999 !not defined
       sides(is)%orient_l = -1
       sides(is)%orient_r = -9999 !not defined
     end if !BCtype(3)=10

     !inner sides (x-minus side of each element)
     do ie1=2,ne(1)
       ie = ie1 + (ie2-1)*ne(1)
       is=is+1
       sides(is)%e_l = (ie1-1) + (ie2-1)*ne(1) ! left neighbor 
       sides(is)%e_r = ie                      ! current element
       sides(is)%iloc_l = 2
       sides(is)%iloc_r = 4
       sides(is)%orient_l = +1
       sides(is)%orient_r = -1
     end do !ie1=2,ne(1)
     
     !right BC
     if(BCtype(4).ne.10)then !left&right not periodic
       ie1=ne(1)
       ie = ie1 + (ie2-1)*ne(1)
       is=is+1
       sides(is)%e_l = ie            ! current element
       sides(is)%e_r = -4            ! NEGATIVE: points to BCtype(4)
       sides(is)%iloc_l = 2
       sides(is)%iloc_r = -9999 !not defined
       sides(is)%orient_l = +1
       sides(is)%orient_r = -9999 !not defined
     end if !bctype(4) /=10
   end do !ie2=1,ne(2)

   ! 2) sides with normal parallel to the x2 axis
   do ie1=1,ne(1)
     !lower BC
     ie2=1
     ie = ie1 + (ie2-1)*ne(1)
     if(BCtype(1).eq.10)then !lower boundary is periodic
       is=is+1
       sides(is)%e_l = ne(1)*(ne(2)-1) + ie1 ! lower element (periodic!)
       sides(is)%e_r = ie                    ! current
       sides(is)%iloc_l = 3
       sides(is)%iloc_r = 1
       sides(is)%orient_l = -1
       sides(is)%orient_r = +1
     else
       is=is+1
       sides(is)%e_l = ie            ! current element
       sides(is)%e_r = -1            ! NEGATIVE: points to BCtype(1)
       sides(is)%iloc_l = 1
       sides(is)%iloc_r = -9999 !not defined
       sides(is)%orient_l = +1  
       sides(is)%orient_r = -9999 !not defined
     end if !BCtype(1)=10

     !inner sides, y-minus element side
     do ie2=2,ne(2)
       ie = ie1 + (ie2-1)*ne(1)
       is=is+1
       sides(is)%e_l = ie1 + (ie2-2)*ne(1)    !lower element
       sides(is)%e_r = ie                     !current element
       sides(is)%iloc_l = 3
       sides(is)%iloc_r = 1
       sides(is)%orient_l = -1
       sides(is)%orient_r = +1
     end do !ie2=2,ne(2)

     !upper BC
     if(BCtype(2).ne.10)then !lower&upper are not periodic
       ie2=ne(2)
       ie = ie1 + (ie2-1)*ne(1)
       is=is+1
       sides(is)%e_l = ie            ! current element
       sides(is)%e_r = -2            ! NEGATIVE: points to BCtype(2)
       sides(is)%iloc_l = 3
       sides(is)%iloc_r = -9999 !not defined
       sides(is)%orient_l = -1  
       sides(is)%orient_r = -9999 !not defined
     end if !BCtype(2) /= 10
   end do !ie1=1,ne(1)

   if(is.ne.grid%ns) stop 'PROBLEM number of sides (grid%ns)'

   ! allocate and define the element array
   allocate( t_e2d_unreg::grid%e( grid%ne ) )
   select type( elems => grid%e ); type is(t_e2d_unreg)

   do ie=1,grid%ne
     ! each element has four sides
     allocate( elems(ie)%s(4) )
   end do
   ! loop over the sides to fill the element information
   do is=1,grid%ns
     ! left element
     ie   = sides(is)%e_l
     iloc = sides(is)%iloc_l
     elems(ie)%s(iloc) = is
     ! right element
     ie   = sides(is)%e_r
     if( ie .gt. 0 ) then !no boundary condition!
       iloc = sides(is)%iloc_r
       elems(ie)%s(iloc) = is
     end if !ie>0
   end do

   ! geometry, first defined on [0,1]^2
   do ie1=1,ne(1)
     do ie2=1,ne(2)
       ie = ie1 + (ie2-1)*ne(1)
       !lower left corner of element in domain [0,1]^2
       elems(ie)%v(1,:) = [real(ie1-1,wp)/real(ne(1),wp),real(ie2-1,wp)/real(ne(2),wp)]
       !lower right corner of element in domain [0,1]^2
       elems(ie)%v(2,:) = [real(ie1,wp)/real(ne(1),wp),  real(ie2-1,wp)/real(ne(2),wp)]
       !upper right corner of element in domain [0,1]^2
       elems(ie)%v(3,:) = [real(ie1,wp)/real(ne(1),wp),  real(ie2,wp)/real(ne(2),wp)]
       !upper left corner  of element in domain [0,1]^2
       elems(ie)%v(4,:) = [real(ie1-1,wp)/real(ne(1),wp),real(ie2,wp)/real(ne(2),wp)]

     end do
   end do

   !global mapping: map all element corners 
   !from reference domain  [0,1]^2 --> physical domain defined in domainType

   select case(domainType) 
       case('rectangular') 
            call linear_domain_map(grid,ne, domainParameters(1:4,:))
       case('rectangular_dist')
            call linear_domain_map_distorted(grid,ne,domainParameters(1:4,:))
       case('parabolic') 
            call parabolic_domain_map(grid,ne, domainParameters(1:5,:))
       case('half_cylinder') 
            call half_cylinder_domain_map(grid,ne,domainParameters(1,:))
       case('full_cylinder')
            call full_cylinder_domain_map(grid,ne,domainParameters(1,:))
   end select 

     !fill side geometry, with left element normal and side length
   do is=1,grid%ns
     ! left element
     ie   = sides(is)%e_l
     iloc = sides(is)%iloc_l

     select case (iloc)
     case(1)
       associate( dx => (elems(ie)%v(2,1) - elems(ie)%v(1,1)),&
                  dy => (elems(ie)%v(2,2) - elems(ie)%v(1,2))) 
       sides(is)%length = sqrt(dx**2 + dy**2) 

       sides(is)%n= 1.0_wp/(sides(is)%length) * [dy, -dx ]  
       end associate
     case(2)
       associate( dx => (elems(ie)%v(3,1) - elems(ie)%v(2,1)),&
                  dy => (elems(ie)%v(3,2) - elems(ie)%v(2,2))) 
      
       sides(is)%length = sqrt(dx**2 + dy**2) 
       sides(is)%n= 1.0_wp/(sides(is)%length) * [dy, -dx ]  
       end associate
     case(3)  
       associate( dx => (elems(ie)%v(4,1) - elems(ie)%v(3,1)),&
                  dy => (elems(ie)%v(4,2) - elems(ie)%v(3,2))) 
       sides(is)%length = sqrt(dx**2 + dy**2) 

       sides(is)%n= 1.0_wp/sides(is)%length * [dy, -dx ]  
       end associate
     case(4)
       associate( dx => (elems(ie)%v(1,1) - elems(ie)%v(4,1)),&
                  dy => (elems(ie)%v(1,2)-elems(ie)%v(4,2))) 
       sides(is)%length = sqrt(dx**2 + dy**2) 
       sides(is)%n= 1.0_wp/sides(is)%length * [dy, -dx ]  
       end associate
     end select !iloc
   end do !is=1,grid%ns

   end select !element type
   end select !side type

 end subroutine new_2d_grid_unreg
 
!-----------------------------------------------------------------------------
!-------------------- GRID ROUTINES ------------------------------------------

 !> evaluate the normal vector at side s and position \( x \in [-1;1] \).
 !!
 pure subroutine get_n_2d(n, s,x)
  !---------------------------------------------------------------------------
  !input variables
  class(t_s2d), intent(in)  :: s      !! side
  real(wp),     intent(in)  :: x(:,:) !! evaluation position [-1;1]
  !output variable
  real(wp),     intent(out) :: n(:,:) !! normal vector 
  !---------------------------------------------------------------------------

   ! the normal is the same at all the points (straight sides)
   n(1,:) = s%n(1)
   n(2,:) = s%n(2)

 end subroutine get_n_2d

!-----------------------------------------------------------------------------

 !> evaluate the surface element of side s, \( |e|/|\tilde{e}| \).
 !! since side is linear, only the side length
 !!
 pure subroutine get_sdetj_2d(detj, s,x)
  !---------------------------------------------------------------------------
  !input variables
  class(t_s2d), intent(in)  :: s
  real(wp),     intent(in)  :: x(:,:)
  !output variable
  real(wp),     intent(out) :: detj(:)
  !---------------------------------------------------------------------------
   ! the determinant is the ratio |e|/|\tilde{e}|
   detj = s%length / length_e_tilde

 end subroutine get_sdetj_2d


!----------------------------------------------------------------------------

 !> inverse Jacobian of the element mapping 
 !!\[
 !!  \mathcal{J}_{ij}=\frac{\partial \tilde{x}_j}{\partial x_i} 
 !!\]
 !! \((\mathcal{J})=({J})^{-1}\), can be computed as the inverse of the Jacobian matrix 
 !!\[
 !!  {J}_{ij}=\frac{\partial {x}_j}{\partial \tilde{x}_i} 
 !!\]
 !! \(i,j=1,2\).
 !!
 pure subroutine get_j_it_2d_unreg(j_it, e,x)
  !---------------------------------------------------------------------------
  !input variables
  class(t_e2d_unreg), intent(in)  :: e
  real(wp),     intent(in)  :: x(:,:)
  !output variable
  real(wp),     intent(out) :: j_it(:,:,:)
  !local variables 
  real(wp) ::  dphi(2,4,size(x,2))  !dphi(i,j,:) derivative of the jth lagrangian basis in the ith coordinate 
  real(wp) ::  J(2,2), detJ
  integer  :: i                     !loop index
!---------------------------------------------------------------------------
   
   dphi(1,1,:) = -0.25_wp*(1-x(2,:))
   dphi(1,2,:) =  0.25_wp*(1-x(2,:))
   dphi(1,3,:) =  0.25_wp*(1+x(2,:))
   dphi(1,4,:) = -0.25_wp*(1+x(2,:))

   dphi(2,1,:) = -0.25_wp*(1-x(1,:))
   dphi(2,2,:) = -0.25_wp*(1+x(1,:))
   dphi(2,3,:) =  0.25_wp*(1+x(1,:))
   dphi(2,4,:) =  0.25_wp*(1-x(1,:))

   j_it = 0.0_wp
   J    = 0.0_wp
   
   
   do i = 1, size(x,2)
     !Computes J at each point provided: 
     J(1,1) = dot_product(dphi(1,:,i), e%v(:,1))
     J(1,2) = dot_product(dphi(2,:,i), e%v(:,1)) 
     J(2,1) = dot_product(dphi(1,:,i), e%v(:,2))
     J(2,2) = dot_product(dphi(2,:,i), e%v(:,2))

     !Invert J and transpose to get the inverse transpose Jacobian 
     detJ = J(1,1)*J(2,2) - J(1,2)*J(2,1)
     j_it(1,1,i) =  1.0_wp/detJ*J(2,2)
     j_it(1,2,i) = -1.0_wp/detJ*J(2,1)
     j_it(2,1,i) = -1.0_wp/detJ*J(1,2) 
     j_it(2,2,i) =  1.0_wp/detJ*J(1,1) 
   end do
 
  
 end subroutine get_j_it_2d_unreg

!-----------------------------------------------------------------------------

 !> determinant of the Jacobian of the element mapping \( \det(J) \)
 !!\[
 !!  \det(J)=\frac{\partial x_1}{\partial \tilde{x}_1}
 !!          \frac{\partial x_2}{\partial \tilde{x}_2}
 !!         -\frac{\partial x_2}{\partial \tilde{x}_1}
 !!          \frac{\partial x_1}{\partial \tilde{x}_2}
 !!\]
 !!
pure subroutine get_detj_2d_unreg(detj, e,x)
  !---------------------------------------------------------------------------
  !input variables
  class(t_e2d_unreg), intent(in)  :: e
  real(wp),     intent(in)  :: x(:,:)
  !output variable
  real(wp),     intent(out) :: detj(:)
  !local variables 
  real(wp) ::  dphi(2,4,size(x,2))  !dphi(i,j,:) derivative of the jth lagrangian basis in the ith coordinate 
  real(wp) ::  J(2,2)
  integer  :: i                     !loop index
!---------------------------------------------------------------------------

   dphi(1,1,:) = -0.25_wp*(1-x(2,:))
   dphi(1,2,:) =  0.25_wp*(1-x(2,:))
   dphi(1,3,:) =  0.25_wp*(1+x(2,:))
   dphi(1,4,:) = -0.25_wp*(1+x(2,:))

   dphi(2,1,:) = -0.25_wp*(1-x(1,:))
   dphi(2,2,:) = -0.25_wp*(1+x(1,:))
   dphi(2,3,:) =  0.25_wp*(1+x(1,:))
   dphi(2,4,:) =  0.25_wp*(1-x(1,:))

   J    = 0.0_wp
   
   
   do i = 1, size(x,2)
     !Computes J at each point provided: 
     J(1,1) = dot_product(dphi(1,:,i), e%v(:,1))  !dx/dxi
     J(1,2) = dot_product(dphi(2,:,i), e%v(:,1))  !dx/deta
     J(2,1) = dot_product(dphi(1,:,i), e%v(:,2))  !dy/dxi
     J(2,2) = dot_product(dphi(2,:,i), e%v(:,2))  !dy/deta
     
     !Compute determinant
     detj(i) = J(1,1)*J(2,2) - J(1,2)*J(2,1)
   end do
 end subroutine get_detj_2d_unreg

!-----------------------------------------------------------------------------

 !> evaluate element mapping between physical and reference space
 !! maps points \( \tilde{x}= (\tilde{x}_1, \tilde{x}_2\) in the reference space \([-1, 1]^2\) to a point \(x\)
 !!  in the quadrilateral element defined by the four vertices
 !!  \(\vec{v_1},\vec{v_2},\vec{v_3}, \vec{v_4}\). 
 !! To do this, we use bilinear lagrangian polynomials
 !! \[
 !! \varphi_1 = \frac{1}{4}(1-\tilde{x}_1)(1-\tilde{x}_2) 
 !! \]
 !! \[
 !! \varphi_2 = \frac{1}{4}(1+\tilde{x}_1)(1-\tilde{x}_2) 
 !! \]
 !! \[
 !! \varphi_3 = \frac{1}{4}(1+\tilde{x}_1)(1+\tilde{x}_2) 
 !! \]
 !! \[
 !! \varphi_4 = \frac{1}{4}(1-\tilde{x}_1)(1+\tilde{x}_2) 
 !! \]
 !! Then the mapping is 
 !! \[
 !!  x = \sum_{i=1}^4 \varphi_i(\tilde{x})\vec{v_i}
 !! \]
 !!
 pure subroutine map_2d_unreg(x, e,x_tilde)
  !---------------------------------------------------------------------------
  !input variables
  class(t_e2d_unreg), intent(in)  :: e
  real(wp),     intent(in)  :: x_tilde(:,:)
  !output variable
  real(wp),     intent(out) :: x(:,:)
  ! local variables 
  real(wp) :: phi(4,size(x_tilde,2)) 
  integer  :: i  
!---------------------------------------------------------------------------
   phi(1,:) = 0.25_wp*(1-x_tilde(1,:))*(1-x_tilde(2,:))
   phi(2,:) = 0.25_wp*(1+x_tilde(1,:))*(1-x_tilde(2,:))
   phi(3,:) = 0.25_wp*(1+x_tilde(1,:))*(1+x_tilde(2,:))
   phi(4,:) = 0.25_wp*(1-x_tilde(1,:))*(1+x_tilde(2,:))

   x(1,:)   = 0.0_wp 
   x(2,:)   = 0.0_wp 

   do i=1,4 
      x(1,:) = x(1,:) + phi(i,:)*e%v(i,1) 
      x(2,:) = x(2,:) + phi(i,:)*e%v(i,2) 
   end do  
 end subroutine map_2d_unreg


!-----------------DOMAIN MAPPINGS -------------------------------------
!-----------------------------------------------------------------------
!> Maps to a quadrilateral domain 
 subroutine linear_domain_map(grid, ne, domainParameters)
   class(t_grid), intent(inout) :: grid
   integer,      intent(in)    :: ne(2)
   real(wp),      intent(in)    :: domainParameters(4,2)

   integer  :: ie1, ie2, ie, i

   associate (v => domainParameters) 
   select type(e => grid%e); type is (t_e2d_unreg)
 do ie1=1,ne(1)
     do ie2=1,ne(2)
       ie = ie1 + (ie2-1)*ne(1)
       do i =1,4
       e(ie)%v(i,:) = v(1,:) + (v(2,:)-v(1,:))*e(ie)%v(i,1) + (v(4,:)-v(1,:))*e(ie)%v(i,2)  &
                      +  (v(3,:) - v(1,:) - (v(2,:)-v(1,:) + v(4,:) - v(1,:)))*e(ie)%v(i,1)*e(ie)%v(i,2)
       end do 
     end do
   end do
   end select
   end associate 
 end subroutine linear_domain_map

 !-----------------------------------------------------------------------
 !> Maps to a rectangular domain where the inner elements are slightly distorted, 
 !! mainly used to find a bug  
subroutine linear_domain_map_distorted(grid, ne, domainParameters)
   class(t_grid), intent(inout) :: grid
   integer,      intent(in)    :: ne(2)
   real(wp),      intent(in)    :: domainParameters(4,2)

   integer  :: ie1, ie2, ie, i
   real     :: xtemp,ytemp
   real     :: delta = 0.01_wp
   real(wp), parameter          :: pi = 3.141592653589_wp
   associate (v => domainParameters) 
   select type(e => grid%e); type is (t_e2d_unreg)
 do ie1=1,ne(1)
     do ie2=1,ne(2)
       ie = ie1 + (ie2-1)*ne(1)
       do i =1,4
       xtemp = e(ie)%v(i,1) + delta*sin(2.0_wp*pi*e(ie)%v(i,1))*cos(2.0_wp*pi*e(ie)%v(i,2))
       ytemp = e(ie)%v(i,2) + delta*cos(2.0_wp*pi*e(ie)%v(i,1))*sin(2.0_wp*pi*e(ie)%v(i,2))
       e(ie)%v(i,1) = v(1,1) + (v(2,1)-v(1,1))*xtemp 
       e(ie)%v(i,2) = v(1,2) + (v(4,2)-v(1,2))*ytemp
       end do 
     end do
   end do
   end select
   end associate 
 end subroutine linear_domain_map_distorted


!-----------------------------------------------------------------------
 !> Maps elements on reference domain \( [0,1]^2 \) onto a domain with 
!! parabolic upper and lower bounderies.The domain is specified by its
!! four corners, and the parabolic curves' distance from the upper and 
!! lower line of the respective rectangular domain.  The followin map is used: 
!! <br>
!! \[
!! y_bot = \eta_1 - 4 d_{\rm{bot}}(\xi^2-1) 
!! \]
!! \[
!! y_top = \eta_4 + 4 d_{\rm{top}}(\xi^2-1)
!! \]
!! where \(\eta_1\) and \(\eta_4\) is the value of the domain corner 1 and 4 in 
!! the second dimension  
 subroutine parabolic_domain_map(grid, ne, domainParameters)
   class(t_grid), intent(inout) :: grid
   integer,      intent(in)    :: ne(2)
   real(wp),      intent(in)    :: domainParameters(5,2) !dP(1:4,2) are the corners of the domain, 
                                                         !dp(5,2)   are d_bot, d_top
   real(wp)                     :: y_top,y_bot,dy

   integer  :: ie1, ie2, ie, i
   
   associate(d_bot => domainParameters(5,1), & 
             d_top => domainParameters(5,2), & 
             v     => domainParameters(1:4,:)) 
  
   select type(e => grid%e); type is (t_e2d_unreg)
    do ie1=1,ne(1)
     do ie2=1,ne(2)
       ie = ie1 + (ie2-1)*ne(1)
       do i =1,4
         y_bot = v(1,2) - d_bot*4.0_wp*((e(ie)%v(i,1))*(e(ie)%v(i,1)-1.0_wp))
         y_top = v(4,2) + d_top*4.0_wp*((e(ie)%v(i,1))*(e(ie)%v(i,1)-1.0_wp))
         dy = y_top-y_bot
         e(ie)%v(i,1) = v(1,1) + (v(2,1)-v(1,1))*e(ie)%v(i,1) 
         e(ie)%v(i,2) = y_bot + dy*e(ie)%v(i,2) 
       end do 
     end do
    end do
   end select
  end associate 
 
 end subroutine parabolic_domain_map

!-----------------------------------------------------------------------
 !> Maps elements on reference domain \( [0,1]^2 \) to a half disc, with 
!! a benchmark  in the middle. Can be used to simulate a potential flow, 
!! where the analytical solution tells us that the  velocity above the
!!  obstacle should be 2 times the inflow velocity, 
!! <br>
!! The element density is higher close to the obstacle.  
!! The domain is specified by an inner and outer radius, 
!! domainParameter \( = (r_i, r_o) \).  It is used a spherical map as follows:  
!! \[
!! \phi = \pi - \pi*\xi
!! \] 
!! \[
!! r = r_i + (r_o - r_i)*\eta^{1.3}
!! \]
!! \[
!! x = r*\cos (\phi)
!! \]
!! \[
!! y = r*\sin (\phi)
!! \]
subroutine half_cylinder_domain_map(grid, ne, domainParameters)
   class(t_grid), intent(inout) :: grid
   integer,      intent(in)    :: ne(2)
   real(wp),      intent(in)    :: domainParameters(1,2) !inner radius, outer radius 
  
   !local variables  
   real(wp)                     :: phi, r
   real(wp), parameter          :: pi = 3.141592653589_wp
   integer  :: ie1, ie2, ie, i

   associate(inner_r => domainParameters(1,1), &
             outer_r => domainParameters(1,2)) 
   select type(e => grid%e); type is (t_e2d_unreg)
    do ie1=1,ne(1)
     do ie2=1,ne(2)
       ie = ie1 + (ie2-1)*ne(1)
       do i =1,4
         phi = pi-pi*e(ie)%v(i,1)
         r = inner_r + (outer_r-inner_r)*(e(ie)%v(i,2))**1.3_wp
         e(ie)%v(i,1) = r*cos(phi)
         e(ie)%v(i,2) = r*sin(phi)
       end do 
     end do
    end do
   end select

   end associate
 end subroutine half_cylinder_domain_map 

!-----------------------------------------------------------------------

!> Maps elements on reference domain \( [0,1]^2 \) to a disc, with 
!! a cylindrical obstacle in the middle. Can be used to simulate a 
!! Karman vortex street
!! <br>
!! To avoid unstabilities for high Reynolds numbers (Re > 50), the element 
!! density is higher close to, and to the right of the obstacle. It is 
!! assumed that the flow enters from the right for this to be a good solution.
!! The domain is specified by an inner and outer radius, 
!! domainParameter \( = (r_i, r_o) \).  It is used a spherical map as follows:  
!! \[
!! \phi = \pi - 4*\arctan{(2 \xi -1)}
!! \] 
!! \[
!! r = r_i + (r_o - r_i)*\eta^{1.5}
!! \]
!! \[
!! x = r*\cos (\phi)
!! \]
!! \[
!! y = r*\sin (\phi)
!! \]
 subroutine full_cylinder_domain_map(grid, ne, domainParameters)
   class(t_grid), intent(inout) :: grid
   integer,      intent(in)    :: ne(2)
   real(wp),      intent(in)    :: domainParameters(1,2) !inner radius, outer radius 
  
   !local variables  
   real(wp)                     :: phi, r
   real(wp), parameter          :: pi = 3.141592653589_wp
   integer  :: ie1, ie2, ie, i

   associate(inner_r => domainParameters(1,1), &
             outer_r => domainParameters(1,2)) 
   select type(e => grid%e); type is (t_e2d_unreg)
    do ie1=1,ne(1)
     do ie2=1,ne(2)
       ie = ie1 + (ie2-1)*ne(1)
       do i =1,4
       ! OBS: Important to keep the right domain orientation => right element orientation
        ! phi =-2.0_wp*pi*(e(ie)%v(i,1)+e(ie)%v(i,1)**3)*0.5_wp
        ! phi =-2.0_wp*pi*e(ie)%v(i,1)
        phi =pi- 4.0_wp*atan(-1.0_wp+2.0*e(ie)%v(i,1))
         r = inner_r + (outer_r-inner_r)*(e(ie)%v(i,2))**1.5_wp
         e(ie)%v(i,1) = r*cos(phi)
         e(ie)%v(i,2) = r*sin(phi)
       end do 
     end do
    end do
   end select

   end associate
 end subroutine full_cylinder_domain_map 


end module mod_2d_grid
